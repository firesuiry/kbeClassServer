# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *

class Hall(KBEngine.Entity):
    def __init__(self):
        DEBUG_MSG("Hall init")
        KBEngine.Entity.__init__(self)

        # 储存大厅
        KBEngine.globalData["Halls"] = self

        # 存放所有正在匹配玩家ENTITYCALL
        self.OnMarchingPlayer = []

        self.addTimer(6, 3, 1)


    def reqAddMarcher(self, player):
        # 此函数添加匹配玩家入列表
        DEBUG_MSG("Account[%i].reqAddMarcher:" % player.id)

        if player in self.OnMarchingPlayer:
            return

        self.OnMarchingPlayer.append(player)

    def reqDelMarcher(self, player):

        # 此函数删除匹配玩家从列表
        DEBUG_MSG("Account[%i].reqDelMarcher:" % player.id)
        if player not in self.OnMarchingPlayer:
            return

        self.OnMarchingPlayer.remove(player)

    def march(self):
        #DEBUG_MSG("Hall.march:marcherSum:[%s]" % len(self.OnMarchingPlayer))
        if len(self.OnMarchingPlayer) > 1:
            players = [self.OnMarchingPlayer[0],self.OnMarchingPlayer[1]]
            self.marchSuccess(players)
            del self.OnMarchingPlayer[1]
            del self.OnMarchingPlayer[0]




    def marchSuccess(self,players):
        DEBUG_MSG("Hall.marchSuccess: playerIDs:[%s]" % players)

        prarm = {
            "player0": players[0],
            "player1": players[1]
        }

        BattleField = KBEngine.createEntityAnywhere("BattleField", prarm)

    def onTimer(self, id, userArg):
        """
        KBEngine method.
        使用addTimer后， 当时间到达则该接口被调用
        @param id		: addTimer 的返回值ID
        @param userArg	: addTimer 最后一个参数所给入的数据
        """
        #DEBUG_MSG(id, userArg)
        if userArg == 1:
            self.march()