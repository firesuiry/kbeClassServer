# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000002(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------


	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	def onUse(self,targetID,selfPos):
		skillBase.onUse(self,targetID,selfPos)
		entity = self.getEntityByID(targetID)
		if entity == None:
			return

		entity.recvHeal(self.id,2)