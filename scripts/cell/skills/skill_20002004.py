# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_20002004(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onRoundEnd(self,isSelf):
		#super().onRoundEnd(isSelf)
		if isSelf:
			ls = self.getSelfFollowerAndHeroList(False)
			for e in ls:
				self.causeHeal(e,1)


	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	