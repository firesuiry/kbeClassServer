# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000198(skillBase):
	#卡牌名称：救赎
	#卡牌描述：<b>奥秘：</b>当一个你的随从死亡时，使其回到战场，并具有1点生命值。
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onFollowerDie(self,followerEntity):
		if followerEntity.playerID == self.playerID:
			if len(self.followerList) <= 6:
				followerEntity.HP = 1
				self.avatar.followerPosAssigned(followerEntity.id,6)
			self.changePos('USED')


	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	