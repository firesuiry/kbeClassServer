# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase
import random

class skill_10001487(skillBase):
	#卡牌名称：砰砰机器人
	#卡牌描述：<b>亡语：</b>对一个随机敌人造成1到4点伤害。
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onDead(self):
		damage = random.randint(1,4)
		self.randomCauseDamageToOppo(damage)

	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	