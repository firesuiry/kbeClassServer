# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000168(skillBase):
	#卡牌名称：烈焰小鬼
	#卡牌描述：<b>战吼：</b>对你的英雄造成3点伤害。
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onUse(self,targetID,selfPos):
		#super(skill_10000168, self).onUse(targetID,selfPos)
		self.causeDamage(self.avatarID,3)

	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	