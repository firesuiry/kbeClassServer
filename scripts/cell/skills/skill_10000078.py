# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000078(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onUse(self,targetID,selfPos):
		#super(skill_10000078, self).onUse(targetID,selfPos)
		self.causeDamage(targetID,1)
		self.frozenTarget(targetID)

	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	