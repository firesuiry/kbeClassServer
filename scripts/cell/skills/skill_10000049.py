# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000049(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onUse(self,targetID,selfPos):
		skillBase.onUse(self,targetID,selfPos)
		target = self.getEntityByID(targetID)
		if target == None:
			return
		params = {
			'attAdd':2,
			'targetEntity':target
		}
		self.creatBuff(params)
		target.rush()

	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	