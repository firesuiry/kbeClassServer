# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000162(skillBase):
	#卡牌名称：飞刀杂耍者
	#卡牌描述：每当你召唤一个随从时，对一个随机敌方角色造成1点伤害。
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------

	def onSummonFollower(self,followerEntityID):
		#super(skill_10000162, self).onSummonFollower(followerEntityID)
		self.randomCauseDamageToOppo(1)
	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	