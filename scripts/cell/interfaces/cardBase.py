# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
import d_card_dis
import random
import copy
from interfaces.spell import spell
#from interfaces.Buff import Buff

class cardBase(KBEngine.Entity,spell):
	def __init__(self):
		KBEngine.Entity.__init__(self)
		'''
		self.pos
		HAND		手牌
		KZ			卡组中
		0-6			场上1-7号
		HERO		英雄
		WEAPON		武器
		SKILL		技能
		DEAD		死过的卡（随从）
		USED		用过的卡（法术）
		SECRRET		奥秘
		BUFF        BUFF环境buff分发牌
		'''		
		if self.cardID != 0:
			self.initProperty()

	def initProperty(self):
		cardID = self.cardID

		self.cost = d_card_dis.datas[cardID]["cost"]
		self.att = d_card_dis.datas[cardID]["att"]
		self.HP = d_card_dis.datas[cardID]["HP"]
		self.maxHP = d_card_dis.datas[cardID]["HP"]
		effectBool = d_card_dis.datas[cardID]["effectBool"]
		self.isTaunt = int( effectBool[0:1])
		self.isRush = int(effectBool[1:2])
		self.isWindfury = int( effectBool[2:3])
		self.isDivineShield = int( effectBool[3:4])
		self.isAbledForever = int(effectBool[4:5])
		if self.isAbled == 0:
			self.isAbled = int(effectBool[1:2])
		self.isStealth = int( effectBool[5:6])
		self.frozen = 0
		self.immune = 0
		self.race = d_card_dis.datas[cardID].get('race','')
		self.envBuff = d_card_dis.datas[cardID].get('envBuff',0)

		self.attSum = 0
		self.type = d_card_dis.datas[cardID]["type"]
		self.poison = d_card_dis.datas[cardID]["poison"]
		self.suckBlood = d_card_dis.datas[cardID]["suckBlood"]

		self.buffList = []
		self.sendBuffList = []

		self.chooseDataStore = {
			'targetID': 0,
			'needPos': 0,
			'active': False
		}
		DEBUG_MSG('card.cell::__init__: [%i]  cardID:[%s]' % (self.id,self.cardID))
		spell.__init__(self, cardID)

	def changePos(self,pos):
		DEBUG_MSG('card.cell::changePos: [%i] pos:[%s]' % (self.id,pos))
		self.pos = pos

	def reqUse(self,callerID,targetID):
		DEBUG_MSG('card.cell::reqUse: [%i] targetID:[%s]' % (self.id,targetID))

		if not self.avatar.useCrystal(int(self.cost)):
			pass

		if self.pos == 'HAND':
			if self.type == 3:#法术1奥秘2怪兽3武器4
				self.avatar.followerPosAssigned(self)
				self.isAbled = self.isRush
				self.onUse(targetID,self.pos)
				#self.battlefiled.onSummonFollower(self.id)
			elif self.type == 4:
				self.changePos('WEAPON')
				self.onUse(targetID,'WEAPON')
			elif self.type == 2:
				self.changePos('SECRET')
				self.onUse(targetID,'SECRET')
			elif self.type == 1:
				self.changePos('USED')
				self.onUse(targetID,'SPELL')
		elif self.pos == 'SKILL':
			self.onUse(targetID,'SKILL')


	def reqAtt(self,callerID,targetID):
		DEBUG_MSG('card.cell::reqAtt: [%i] targetID:[%s]' % (self.id,targetID))
		entity = self.getEntityByID(targetID)

		entity.recvAtt(self)

	def recvAtt(self,srcEntity):
		DEBUG_MSG('card.cell::recvAtt: [%i] srcEntity:[%s]' % (self.id, srcEntity.id))

		self.recvDamage(srcEntity.att,srcEntity)
		if self.att > 0:
			srcEntity.recvDamage(self.att,self)


	def recvDamage(self,damageSum,srcEntity):
		DEBUG_MSG('card.cell::recvDamage: [%i]  damageSum[%s] srcEntity:[%s]  ' % (self.id,damageSum, srcEntity.id))

		self.HP = self.HP - damageSum
		self.HP = self.HP
		if self.HP <= 0:
			self.die()


	def die(self):
		DEBUG_MSG('card.cell::die: [%i]' % (self.id))
		self.changePos('DEAD')
		self.avatar.followerDie(self)

	def getEntityByID(self,ID):
		entity = KBEngine.entities.get(ID,None)
		return entity














